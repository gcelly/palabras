package palabrasdos;

import java.util.Scanner;

public class PalabrasDos {

    private static String palabra;
    private static int tamanioPalabra;
    private static int tamanio;
    static int op = 1;

    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);
        String c = "a";
        while (op == 1) {
            System.out.println("Ingrese caracter");
            c = scan.next();
            calcular(c);
        }
    }

     private static String mayuscula(String x) {
        return x.toUpperCase();
        
    }
    public static void calcular(String x) {
        int longi = x.length();
        if (longi > 10 && longi < 15) {
            System.out.println(x);
            obtener(x);
            op = 2;
        } else {
            System.out.println("Ingresar caracter con digitos mayores a 10 y menores a quince");
            op = 1;
        }
    }

    public static void obtener(String x) {
        int longi = x.length();
        char c = x.charAt(longi - 1);
        System.out.println(c);
    }
}
